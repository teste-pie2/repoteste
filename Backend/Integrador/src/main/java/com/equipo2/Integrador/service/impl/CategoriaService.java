package com.equipo2.Integrador.service.impl;

import com.equipo2.Integrador.exceptions.BadRequestException;
import com.equipo2.Integrador.exceptions.ResourceNotFoundException;
import com.equipo2.Integrador.model.Categoria;
import com.equipo2.Integrador.repository.CategoriaRepository;
import com.equipo2.Integrador.service.IService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class CategoriaService implements IService<Categoria, Long> {

    private final CategoriaRepository categoriaRepository;
    private final Logger logger = Logger.getLogger(CategoriaService.class);

    @Autowired
    public CategoriaService(CategoriaRepository categoriaRepository)
    {
        this.categoriaRepository = categoriaRepository;
    }

    @Override
    public Categoria agregar(Categoria categoria) throws BadRequestException
    {
        if (categoria.getId() != null)
            throw new BadRequestException("El registro de categorías no puede recibir un ID.");
        else
        {
            Categoria categoriaSaved = categoriaRepository.save(categoria);
            logger.info("Se registró una categoría con ID " + categoriaSaved.getId() + ".");
            return categoriaSaved;
        }
    }

    @Override
    public Optional<Categoria> buscar(Long id)
    {
        return categoriaRepository.findById(id);
    }

    @Override
    public List<Categoria> listar()
    {
        return categoriaRepository.findAll();
    }

    @Override
    public Categoria actualizar(Categoria categoria) throws BadRequestException, ResourceNotFoundException
    {
        if (categoria.getId() == null)
            throw new BadRequestException("La actualización de categorías requiere de un ID.");
        else if (categoriaRepository.findById(categoria.getId()).isEmpty())
            throw new ResourceNotFoundException("La categoría con el ID " + categoria.getId() + " no existe.");
        else
            return categoriaRepository.save(categoria);
    }

    @Override
    public void eliminar(Long id) throws ResourceNotFoundException
    {
        Optional<Categoria> categoria = categoriaRepository.findById(id);
        if (categoria.isPresent())
        {
            categoriaRepository.deleteById(id);
            logger.info("Se eliminó la categoría con ID " + id + ".");
        }
        else
            throw new ResourceNotFoundException("La categoría con el ID " + id + " no existe.");

    }
}
