package com.equipo2.Integrador.exceptions;

public class BadRequestException extends Exception {

    public BadRequestException(String msg)
    {
        super(msg);
    }
}