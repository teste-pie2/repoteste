package com.equipo2.Integrador.controller;

import com.equipo2.Integrador.exceptions.BadRequestException;
import com.equipo2.Integrador.exceptions.ResourceNotFoundException;
import com.equipo2.Integrador.model.Categoria;
import com.equipo2.Integrador.service.impl.CategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/categorias")
public class CategoriaController {

    private final CategoriaService categoriaService;

    @Autowired
    public CategoriaController(CategoriaService categoriaService)
    {
        this.categoriaService = categoriaService;
    }

    @PostMapping
    public ResponseEntity<Categoria> agregar(@RequestBody Categoria categoria) throws BadRequestException
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(categoriaService.agregar(categoria));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Categoria> buscar(@PathVariable Long id) throws ResourceNotFoundException
    {
        Optional<Categoria> categoriaBuscada = categoriaService.buscar(id);

        if (categoriaBuscada.isPresent())
            return ResponseEntity.status(HttpStatus.OK).body(categoriaBuscada.get());
        else
            throw new ResourceNotFoundException("La categoría con el ID " + id + " no existe.");
    }

    @GetMapping
    public ResponseEntity<List<Categoria>> listar()
    {
        return ResponseEntity.ok(categoriaService.listar());
    }

    @PutMapping
    public ResponseEntity<Categoria> actualizar(@RequestBody Categoria categoria) throws BadRequestException, ResourceNotFoundException
    {
        return ResponseEntity.status(HttpStatus.OK).body(categoriaService.actualizar(categoria));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> eliminar(@PathVariable Long id) throws ResourceNotFoundException
    {
        categoriaService.eliminar(id);

        // Si llega hasta acá es porque no hubo excepción
        return ResponseEntity.status(HttpStatus.OK).body("La categoría con el ID " + id + " fue eliminada exitosamente.");
    }

}
