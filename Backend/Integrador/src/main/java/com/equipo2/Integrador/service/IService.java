package com.equipo2.Integrador.service;

import com.equipo2.Integrador.exceptions.BadRequestException;
import com.equipo2.Integrador.exceptions.ResourceNotFoundException;

import java.util.List;
import java.util.Optional;

public interface IService<T, ID> {

    public T agregar(T t) throws BadRequestException;
    public Optional<T> buscar(ID id);
    public List<T> listar();
    public T actualizar(T t) throws BadRequestException, ResourceNotFoundException;
    public void eliminar(ID id) throws ResourceNotFoundException;
}