package com.equipo2.Integrador.exceptions;

public class ResourceNotFoundException extends Exception {

    public ResourceNotFoundException(String msg)
    {
        super(msg);
    }
}