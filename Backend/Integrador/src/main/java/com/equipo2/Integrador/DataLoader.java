package com.equipo2.Integrador;

import com.equipo2.Integrador.exceptions.BadRequestException;
import com.equipo2.Integrador.model.Categoria;
import com.equipo2.Integrador.service.impl.CategoriaService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("!test")
@Component
public class DataLoader implements ApplicationRunner {

    private CategoriaService categoriaService;
    private Logger logger = Logger.getLogger(DataLoader.class);

    @Autowired
    public DataLoader(CategoriaService categoriaService)
    {
        this.categoriaService = categoriaService;
    }

    public void run(ApplicationArguments args)
    {
        if (categoriaService.listar().isEmpty())
        {
            try {
                categoriaService.agregar(new Categoria("Hotel Urbano", "Hoteles cercanos al centro de la ciudad", "http://www.hotel-urbano.com"));
                categoriaService.agregar(new Categoria("Hotel de Playa", "Hoteles cercanos a balnearios", "http://www.hotel-playa.com"));
                categoriaService.agregar(new Categoria("Hotel en la Naturaleza", "Hoteles cercanos a selvas/bosques/montañas", "http://www.hotel-naturaleza.com"));
            } catch (BadRequestException e) {
                logger.debug("Ha ocurrido un error mientras se registraban las categorías.");
            }
        }
    }
}
