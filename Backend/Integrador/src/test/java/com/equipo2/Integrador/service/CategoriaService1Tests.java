package com.equipo2.Integrador.service;

import com.equipo2.Integrador.exceptions.BadRequestException;
import com.equipo2.Integrador.exceptions.ResourceNotFoundException;
import com.equipo2.Integrador.model.Categoria;
import com.equipo2.Integrador.service.impl.CategoriaService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

@ActiveProfiles("test")
@SpringBootTest
public class CategoriaService1Tests {

    @Autowired
    private CategoriaService categoriaService;

    @BeforeEach
    public void agregaCategorias() throws BadRequestException
    {
        Categoria categoriaCasa = Categoria.builder().titulo("Casa").descripcion("Casas increíbles").urlImagen(
                "http" + "://www.fotos.com").build();
        Categoria categoriaHotel = Categoria.builder().titulo("Hotel").descripcion("Hoteles increíbles").urlImagen(
                "http" + "://www.fotos.com").build();
        categoriaService.agregar(categoriaCasa);
        categoriaService.agregar(categoriaHotel);
    }

    @Test
    public void agregaYListaCategorias(){
        List<Categoria> lista = categoriaService.listar();
        Assertions.assertThat(lista.size()).isGreaterThanOrEqualTo(2);
    }

    @Test
    public void buscaCategoriaPorID(){
        Optional<Categoria> resultado = categoriaService.buscar(2L);
        Assertions.assertThat(resultado).isPresent();
    }

    @Test
    public void buscaPorIDyActualizaCategoria() throws BadRequestException, ResourceNotFoundException
    {
        Categoria categoriaActualizada =
                Categoria.builder().titulo("Hotel").descripcion("Hoteles nuevos").urlImagen("http" +
                        "://www.new.com").build();
        categoriaService.agregar(categoriaActualizada);
        Optional<Categoria> categoria = categoriaService.buscar(categoriaActualizada.getId());
        categoriaService.actualizar(categoriaActualizada);
        Assertions.assertThat(categoria.get().toString()).contains(categoriaActualizada.toString());
    }

    @Test
    public void eliminaCategoriaPorID() throws ResourceNotFoundException, BadRequestException
    {
        Categoria categoriaNueva =
                Categoria.builder().titulo("Hotel").descripcion("Hotel a ser eliminado").urlImagen("http" +
                        "://www.elimineme.com").build();
        categoriaService.agregar(categoriaNueva);
        categoriaService.eliminar(categoriaNueva.getId());
        Assertions.assertThat(categoriaService.buscar(categoriaNueva.getId())).isEmpty();
    }
}
