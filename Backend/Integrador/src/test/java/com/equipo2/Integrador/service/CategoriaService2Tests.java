package com.equipo2.Integrador.service;

import com.equipo2.Integrador.exceptions.BadRequestException;
import com.equipo2.Integrador.exceptions.ResourceNotFoundException;
import com.equipo2.Integrador.model.Categoria;
import com.equipo2.Integrador.service.impl.CategoriaService;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)  // Anotación para resetear le DB antes de cada test (así son independientes)
@ExtendWith(SpringExtension.class)  // Es redundante
@SpringBootTest
public class CategoriaService2Tests {

    @Autowired
    private CategoriaService categoriaService;

    @Test
    public void agregarCategoria() throws BadRequestException
    {
        Categoria categoriaAgregada = categoriaService.agregar(new Categoria("Hotel Urbano", "Hoteles cercanos al centro de la ciudad", "http://www.hotelurbano.com"));
        Assertions.assertNotNull(categoriaAgregada.getId());
    }

    @Test
    public void buscarCategoria() throws BadRequestException
    {
        Categoria categoriaAgregada = categoriaService.agregar(new Categoria("Hotel Urbano", "Hoteles cercanos al centro de la ciudad", "http://www.hotelurbano.com"));
        Optional<Categoria> categoriaBuscada = categoriaService.buscar(categoriaAgregada.getId());
        Assertions.assertTrue(categoriaBuscada.isPresent());
        Assertions.assertEquals(Long.valueOf(1), Long.valueOf((categoriaBuscada.get().getId())));
    }

    @Test
    public void buscarCategoriaInexistente() throws BadRequestException
    {
        categoriaService.agregar(new Categoria("Hotel Urbano", "Hoteles cercanos al centro de la ciudad", "http://www.hotelurbano.com"));
        Optional<Categoria> categoriaBuscada = categoriaService.buscar(2L);
        Assertions.assertTrue(categoriaBuscada.isEmpty());
    }

    @Test
    public void listarTodas() throws BadRequestException
    {
        categoriaService.agregar(new Categoria("Hotel Urbano", "Hoteles cercanos al centro de la ciudad", "http://www.hotelurbano.com"));
        categoriaService.agregar(new Categoria("Hotel de Playa", "Hoteles cercanos a balnearios", "http://www.hoteldeplaya.com"));
        categoriaService.agregar(new Categoria("Hotel en la Naturaleza", "Hoteles cercanos a selvas/bosques/montañas", "http://www.hotelnaturaleza.com"));
        List<Categoria> categorias = categoriaService.listar();
        Assertions.assertTrue(!categorias.isEmpty());
        Assertions.assertTrue(categorias.size() == 3);
    }

    @Test
    public void actualizarCategoria() throws BadRequestException, ResourceNotFoundException
    {
        Categoria categoriaAgregada = categoriaService.agregar(new Categoria("Hotel Urbano", "Hoteles cercanos al centro de la ciudad", "http://www.hotelurbano.com"));
        Optional<Categoria> categoriaBuscada = categoriaService.buscar(categoriaAgregada.getId());
        Assertions.assertTrue(categoriaBuscada.isPresent());
        Assertions.assertEquals("http://www.hotelurbano.com", categoriaBuscada.get().getUrlImagen());

        categoriaBuscada.get().setUrlImagen("http://www.hotelurbano.org");
        Categoria categoriaActualizada = categoriaService.actualizar(categoriaBuscada.get());
        Optional<Categoria> categoriaActualizadaBuscada = categoriaService.buscar(categoriaActualizada.getId());
        Assertions.assertTrue(categoriaActualizadaBuscada.isPresent());
        Assertions.assertEquals("http://www.hotelurbano.org", categoriaActualizadaBuscada.get().getUrlImagen());
    }

    @Test
    public void eliminarCategoria() throws ResourceNotFoundException, BadRequestException
    {
        Categoria categoriaAgregada = categoriaService.agregar(new Categoria("Hotel Urbano", "Hoteles cercanos al centro de la ciudad", "http://www.hotelurbano.com"));
        categoriaService.eliminar(categoriaAgregada.getId());
        Assertions.assertTrue(categoriaService.buscar(categoriaAgregada.getId()).isEmpty());
    }
}