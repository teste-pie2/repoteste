import imagen1 from "./image/hotel.png"
import imagen2 from "./image/hostel.png"
import imagen3 from "./image/depa.png"
import imagen4 from "./image/BedBreakfast.png"

const dataCategorias = [
    {
        img: imagen1,
        titulo: "Hoteles",
        cantidad: "860.021 hoteles",
    },
    {
        img: imagen2,
        titulo: "Hostels",
        cantidad: "12.124 hoteles",
    },
    {
        img: imagen3,
        titulo: "Departamentos",
        cantidad: "412.124 hoteles",
    },
    {
        img: imagen4,
        titulo: "Bed and Breakfast",
        cantidad: "432.324",
    },
 ];

 export default dataCategorias;