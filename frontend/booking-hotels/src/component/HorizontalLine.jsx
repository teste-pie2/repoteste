import React from "react";

// CSS:
import styles from "../styles/horizontalLine.module.css";

const HorizontalLine = () => {
  return (
    <>
      <hr className={styles.horizontalLine} />
    </>
  );
};

export default HorizontalLine;
