import React from "react";
import { Route, Switch } from "react-router-dom";

import Buscador from "./Buscador";
import CardProduct from "./CardProduct";
import SignInForm from "./SignInForm";
import SignUpForm from "./SignUpForm";
import NotFound from "./NotFound";
import DetailsPage from "./detailsPage/DetailsPage";

// CSS
import "../styles/index/main.module.css";

const Main = (props) => {
  return (
    <main>
      <Switch>
        <Route exact path="/">
          <Buscador />
          <CardProduct />
        </Route>
        <Route exact path="/login" component={SignInForm} />
        <Route exact path="/register" component={SignUpForm} />
        <Route exact path="/detailsPage" component={DetailsPage} />
        <Route path="*" component={NotFound} />
      </Switch>
    </main>
  );
};

export default Main;
