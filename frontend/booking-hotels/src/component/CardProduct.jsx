import React from "react";
import { Link } from "react-router-dom";
import DetailsPage from "./detailsPage/DetailsPage";

import tarjetas from "../tarjetas.json";

// CSS
import styles from "../styles/index/cardproduct.module.css";
import globalStyles from "../styles/global.module.css";

const CardProduct = () => {
  return (
    <div className={styles.productsCardContainer}>
      <h2 className={styles.h2}>Recomendaciones</h2>
      <div className={styles.cardContainer}>
        {tarjetas.map((item) => (
          <div key={item.title} className={styles.card}>
            <div className={styles.cardImage}>
              <Link to="./">
                <img src={item.img} alt={item.title} />
              </Link>
            </div>
            <div className={styles.cardText}>
              <h4>{item.category}</h4>
              <h3>{item.title}</h3>
              <p>{item.location}</p>
              <p>{item.description}</p>
              <div className={styles.cardProductButtonContainer}>
                <Link to="./detailsPage">
                  <button className={globalStyles.button}>Ver detalle</button>
                </Link>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default CardProduct;
