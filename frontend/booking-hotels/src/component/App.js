import React from "react";
import Header from "./header/Header";
import Main from "./Main";
import Footer from "./Footer";
import { Route, Link, Router, Switch, BrowserRouter } from "react-router-dom";

// CSS
import globalStyles from "../styles/global.module.css";

function App() {
  return (
    <div className={globalStyles.App}>
      <BrowserRouter>
        <Route path="/" render={(props) => <Header {...props} />} />
        <Main />
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
