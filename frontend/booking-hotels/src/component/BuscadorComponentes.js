import React, { useState, useLayoutEffect } from "react";
import DatePicker, { registerLocale } from "react-datepicker";
import Select, { components } from "react-select";
import options from "../dataCountries.js";
import es from "date-fns/locale/es";
import "react-datepicker/dist/react-datepicker.css";
import stylesbuscador from "../styles/index/buscador.module.css";
import "../styles/index/datepicker.css";
import "../styles/global.module.css";
registerLocale("es", es);

/* ================================================= */
/* ========== Select de Paises y ciudades ========== */
/* ================================================= */

const selectFormatOptionLabel = ({ value, label }) => (
	<div className={stylesbuscador.select_option_list}>
		<i
			className={`far fa-map-marker-alt fa-lg ${stylesbuscador.select_icono_mapa_vacio}`}
			aria-hidden="true"
		/>
		<div className={stylesbuscador.select_option_list_caja}>
			<div className={stylesbuscador.select_option_list_caja_pais}>
				{label.split(",")[0]}
			</div>
			<div className={stylesbuscador.select_option_list_caja_ciudad}>
				{label.split(",")[1]}
			</div>
		</div>
	</div>
);

export const SelectMenu = ({ parentCallback, selectedCity }) => {
	const handleChange = (e) => {
		parentCallback(e.value);
	};

	return (
		<div className={stylesbuscador.select_color}>
			<div
				className={
					selectedCity === ""
						? `${stylesbuscador.oculta}`
						: `${stylesbuscador.selected_city}`
				}
				onClick={() => parentCallback("")}
			>
				<i
					className={`fas fa-map-marker-alt fa-lg ${stylesbuscador.selected_icono_mapa_relleno}`}
					aria-hidden="true"
				/>
				{options.map((option) =>
					option.value === selectedCity ? option.label : ""
				)}
			</div>
			<Select
				value={selectedCity || ""}
				formatOptionLabel={selectFormatOptionLabel}
				styles={selectStyles}
				components={{ ValueContainer }}
				placeholder={"¿A dónde vamos?"}
				className={
					selectedCity
						? `${stylesbuscador.oculta}`
						: `${stylesbuscador.select_container}`
				}
				options={options}
				onChange={handleChange}
				theme={(theme) => ({
					...theme,
					colors: {
						...theme.colors,
						neutral80: "gray",
					},
				})}
			/>
		</div>
	);
};
// ====== Función para configurar el ícono de Mapa en el Select ======
const ValueContainer = ({ children, ...props }) => {
	return (
		components.ValueContainer && (
			<components.ValueContainer {...props}>
				{!!children && (
					<i
						className="fas fa-map-marker-alt"
						aria-hidden="true"
						style={{ position: "absolute", left: 6 }}
					/>
				)}
				{children}
			</components.ValueContainer>
		)
	);
};
// ====== Estilo para dar margin a la izquierda del icono del mapa ======
const selectStyles = {
	valueContainer: (base) => ({
		...base,
		paddingLeft: 24,
	}),
};

/* ================================================= */
/* ========= Check-in Check-out y Calendar ========= */
/* ================================================= */

const min_width_Tablet = 768;
// Función para que re-renderice la página cuando cambia el ancho (viewport) y actualice el calendario para mostrar 1 o 2
function useWindowSize() {
	const [size, setSize] = useState([0, 0]);
	useLayoutEffect(() => {
		function updateSize() {
			setSize([window.innerWidth, window.innerHeight]);
		}
		window.addEventListener("resize", updateSize);
		updateSize();
		return () => window.removeEventListener("resize", updateSize);
	}, []);
	return size;
}
export const Calendar = ({ pull_date, dateRange }) => {
	const [startDate, endDate] = dateRange;
	const [hiddenCheckInOut, setHiddenCheckInOut] = useState(false);
	const [myRef, setMyRef] = useState(false);
	const [screenWidth] = useWindowSize();
	const handlePullDate = (update) => {
		pull_date(update);
	};
	//====== Función del botón Aplicar para cerrar el Calendario ======
	const closeCalendar = () => {
		myRef.setOpen(false);
	};
	//====== Función de la Div con clase "calendar_checkin_out" para abrir el Calendario ======
	const openCalendar = () => {
		myRef.setOpen(true);
	};
	// props.func(startDate, endDate);
	return (
		<div className={stylesbuscador.calendar_container}>
			<div
				className={
					hiddenCheckInOut
						? `${stylesbuscador.oculta}`
						: `${stylesbuscador.calendar_checkin_out}`
				}
				onClick={openCalendar}
			>
				<span
					className={
						hiddenCheckInOut
							? `${stylesbuscador.oculta}`
							: `${stylesbuscador.span_check}`
					}
					onClick={() => setHiddenCheckInOut(true)}
				>
					<i
						className={`far fa-calendar-day ${stylesbuscador.icono_calendar}`}
					></i>
					Check in - Check out
				</span>
			</div>
			<DatePicker
				locale="es"
				dateFormat={"dd" + " 'de' " + "MMM."}
				formatWeekDay={(nameOfDay) =>
					capitalizeFirstLetter(nameOfDay.substr(0, 3))
				}
				monthsShown={`${screenWidth < min_width_Tablet ? "1" : "2"}`}
				minDate={new Date()}
				wrapperClassName={
					!hiddenCheckInOut
						? `${stylesbuscador.oculta}`
						: `${stylesbuscador.textos_calendar}`
				}
				placeholderText="Check-in"
				selectsRange={true}
				startDate={startDate}
				endDate={endDate}
				onChange={handlePullDate}
				//====== Para que no se cierre el calendario al elegir el check-out:======
				shouldCloseOnSelect={false}
				//====== Referencia para función del botón Aplicar en el calendario======
				ref={(r) => {
					setMyRef(r);
				}}
			>
				<div className="vertical-line" />
				<div className={stylesbuscador.calendar_footer}>
					<button
						className={stylesbuscador.calendar_button}
						onClick={closeCalendar}
					>
						Aplicar
					</button>
				</div>
			</DatePicker>
		</div>
	);
};
const capitalizeFirstLetter = (string) =>
	string.charAt(0).toUpperCase() + string.slice(1);
