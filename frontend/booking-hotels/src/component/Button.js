import React from "react";
import { Link } from "react-router-dom";
import "../styles/button.module.css";

const Button = (props) => {
  return (
    <Link
      className={props.className}
      to={props.to}
      aria-current={props.ariaCurrent}
      onClick={props.onClick}
    >
      {props.text}
    </Link>
  );
};

export default Button;
