import React from "react";

// Importación de componentes:
import HeaderDetailsPage from "./HeaderDetailsPage";
import Location from "./Location";
import Maps from "./Maps";

const DetailsPage = () => {
  return (
    <>
      <HeaderDetailsPage />
      <Location />
      <Maps />
    </>
  );
};

export default DetailsPage;
