import React from "react";

// Librería Fontawesome, uso de íconos:
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

// CSS:
import styles from "../../styles/detailsPage/headerDetails.module.css";

const HeaderDetailsPage = () => {
  return (
    <div className={styles.header}>
      <div className={styles.headerText}>
        <h4>Hotel</h4>
        <h3>Hermitage Hotel</h3>
      </div>
      <FontAwesomeIcon
        icon={faChevronLeft}
        //   onClick={}
        className={styles.headerIcon}
      />
    </div>
  );
};

export default HeaderDetailsPage;
