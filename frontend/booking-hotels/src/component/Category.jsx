// import axios from 'axios';

import dataCategorias from "../dataCategorias";
import { Link } from "react-router-dom";

//CSS
import styles from "../styles/index/categories.module.css";

function Category(params) {
  /* =========== Axios a la Api de Categorías ========= */

  // const[listaC, setLista]= useState([])
  //     useEffect(()=> {
  // const listCategories = async () => {
  //    try{
  //       const response = await axios.get(
  //          "http://localhost:8080/categorias"
  //          );
  //          setLista(response.data);
  //       } catch (err){
  //          console.log(err);
  //       }
  //    }
  //    listCategories();
  // },[]);

  /* =========== Lista de categorias hardcodeada ========= */

  return (
    <div className={styles.category}>
      <h2>Buscar por tipo de alojamiento</h2>
      <div className={styles.typeContainer}>
        {dataCategorias.map((item) => (
          <div key={item.titulo} className={styles.typeCard}>
            <Link to="./">
              <img src={item.img} alt={item.titulo} />
            </Link>
            <h3>{item.titulo}</h3>
            <p>{item.cantidad}</p>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Category;
